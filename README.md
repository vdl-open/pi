# Python Virtual data Lake Interface

This is a Python interface to virtual data lakes. Virtual data lakes are open source data platforms 
based on simple triple stores that enable users to integrate data from different sources. 
This package enables a Python client to access the interface
described at https://lacibus.gitlab.io/vdl/org/lacibus/storeapi/StoreApi.html
over the Web.
